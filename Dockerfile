FROM alpine:3.8
MAINTAINER Tao <tao.li.7019@gmail.com>

# Create app user with app group. Specifically assign uid and gid 1000
# to app user and group so that it matches the host user and group id
# on some major host operating systems.
RUN mkdir /app && \
    /usr/sbin/addgroup -g 1000 app && \
    /usr/sbin/adduser -D -H -h /app -u 1000 -G app app && \
    /bin/chown -R app:app /app

# Add dependencies.
RUN apk --no-cache add \
        ca-certificates \
        bash \
        libuuid \
        apr \
        apr-util \
        libjpeg-turbo \
        icu \
        icu-libs \
        pcre \
        zlib \
        libressl libressl-dev \
        nginx \
        supervisor \
        curl \
      && update-ca-certificates

COPY start.sh /
RUN mkdir -p /run/nginx
RUN curl -sLo /usr/local/bin/ep https://github.com/kreuzwerker/envplate/releases/download/v0.0.8/ep-linux && chmod +x /usr/local/bin/ep

ADD ./conf.d /etc/nginx/conf.d
VOLUME ["/var/log/nginx"]

# Separate the logs into their own volume to keep them out of the container.
ADD ca.openssl.conf /ca.openssl.conf
VOLUME ["/var/log/nginx"]

EXPOSE 80 443

# Set nginx directly as the entrypoint.
ENTRYPOINT ["sh", "/start.sh"]
